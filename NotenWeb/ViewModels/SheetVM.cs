﻿using Microsoft.AspNetCore.Mvc.Rendering;
using NotenWeb.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace NotenWeb.ViewModels
{

    /// <summary>
    /// Sheet VM for Price, Duration and Orchestra-Name.
    /// Also creates SelectList (for Edit and Create)
    /// And Saves WriteAccess for Edit-Link in Details
    /// All other Properties are in the underlying SheetModel
    /// </summary>
    /// 
    public class SheetVM
    {

        #region Properties
        public Sheet SheetModel { get; set; }

        [Display(Name = "Orchester")]
        public string OrchestraName { get; set; }


        [DataType(DataType.Time)]
        [Range(typeof(TimeSpan), "00:00:00", "23:59:59",
             ErrorMessage = "Zeit muss zwischen 00:00:00 und 23:59:59 liegen")]
        [Display(Name = "Dauer")]
        public TimeSpan Duration { get; set; }

        [DataType(DataType.Currency)]
        [Range(0, 1000)]
        [Display(Name = "Preis")]
        public decimal Price { get; set; }

        // used to hide links of not PowerUser or Admin (in Details)
        // Note: In IndexSheets (Create, Edit, Delete) WriteAccess of SheetsOrchestrasVM is used
        public bool WriteAccess { get; set; }

        // SelectList in Edit
        //Note: SelectLists compare for strings.
        //Options Archive(string-value 0) and All(string value -1) are added in view.

        public SelectList SelListOrchestras { get; set; }

        // internal list for getting orchestra name out of orchestra Id
        private List<Orchestra> _orchestras { get; set; }


        #endregion


        /// <summary>
        /// Constructor of SheetVM, which uses a sheet as parameter.
        /// Constructor which only uses Orchestras as List for empty SheetVM
        /// Constructor which is parameterless is necessary. This is called, when clicking on Create-Button
        /// </summary>
        /// <param name="sheet">the underlying SheetModel</param>
        /// <param name="Orchestras">SelectList for Orchestras is built</param>


        // gets called when clicking on Create-Button

        public SheetVM()
        {
            SheetModel = new Sheet();
            _orchestras = new List<Orchestra>();
            OrchestraName = "";

            SelListOrchestras = null;
                       
            Duration = TimeSpan.FromSeconds(0);
            Price = 0;
            WriteAccess = false;
        }


        // gets called when opening create-view

        public SheetVM(ref List<Orchestra> Orchestras)
        {
            SheetModel = new Sheet();
            _orchestras = Orchestras;

            // Build SelectList for Orchestras;
            // use LINQ to get list of Orchestras
            IQueryable<string> orchQuery = from o in _orchestras.AsQueryable()
                                           select o.Label;

            SelListOrchestras = new SelectList(orchQuery);

            OrchestraName = "Archiv";

            Duration = TimeSpan.FromSeconds(0);
            Price = 0;
            WriteAccess = false;
        }


        public SheetVM(Sheet sheet, ref List<Orchestra> Orchestras)
        {
            SheetModel = sheet;
            _orchestras = Orchestras;

            // Build SelectList for Orchestras;
            // use LINQ to get list of Orchestras
            IQueryable<string> orchQuery = from o in _orchestras.AsQueryable()
                                           select o.Label;

            SelListOrchestras = new SelectList(orchQuery);


            WriteAccess = false;

            // generate Orchestra Name for this sheet
            bool found = false;

            foreach(Orchestra o in _orchestras)
            {
                if(SheetModel.OrchestraId == o.Id)
                {
                    found = true;
                    OrchestraName = o.Label;
                    break;
                }
            }

            if(!found)
            {
                OrchestraName = "Archiv";
            }


            // generate price:
            if (SheetModel.Price == null)
                Price = 0;
            else
                Price = (decimal)SheetModel.Price / 100;

            // generate duration:
            if (SheetModel.Duration == null)
                Duration = TimeSpan.FromSeconds(0);
            else
                Duration = TimeSpan.FromSeconds((int)SheetModel.Duration);
        }



        /// <summary>
        /// This function converts the Price, Duration and OrchestraName from SheetVM 
        /// back and writes it in the underlying sheet.
        /// Returns whether the orchestra name was found. if not, OrchestraId is set to 0.
        /// </summary>
        /// <param name="Orchestras"></param>

        public bool ConvertBack(ref List<Orchestra> Orchestras)
        {
            // Duration
            if(Duration < TimeSpan.FromSeconds(0))
                Duration = TimeSpan.FromSeconds(0);

            SheetModel.Duration = (int)Duration.TotalSeconds;

            // Price
            if (Price < 0) // this should not happen because of validation
                Price = 0;

            SheetModel.Price = (int)(Price * 100);

            // OrchestraId
            bool found = false;

            foreach (Orchestra o in Orchestras)
            {
                if (OrchestraName == o.Label)
                {
                    found = true;
                    SheetModel.OrchestraId = o.Id;
                    break;
                }
            }

            if (!found)
            {
                SheetModel.OrchestraId = 0;
            }

            return found;
        }


    }
}
