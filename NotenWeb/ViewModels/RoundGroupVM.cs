﻿using System.Collections.Generic;
using System.Linq;

namespace NotenWeb.ViewModels
{

    /// <summary>
    /// RoundGroup VM. Contains a List of RoundVMs and a RoundGroupLabel
    /// </summary>
    /// 
    public class RoundGroupVM
    {

        #region Properties

		public string RoundGroupLabel { get; set; }
        
		public List<RoundVM> ContainedRoundVMs { get; set; }

        #endregion

		
		/// <summary>
		/// Constructor for RoundGroupVM, which needs a RoundGroupLabel
		/// Note: AddRound is only possible, when label fits
		/// </summary>
		/// 
        public RoundGroupVM(string roundgrouplabel)
        {
            RoundGroupLabel = roundgrouplabel;
            ContainedRoundVMs = new List<RoundVM>();
        }

		
		
		/// <summary>
		/// AddRound is only possible, when label fits
		/// </summary>
		/// 
        public bool AddRound(RoundVM rvm)
        {
            if(rvm.RoundModel.Roundgroup == RoundGroupLabel)
            {
                ContainedRoundVMs.Add(rvm);
                return true;
            }
            return false;
        }

        public void SortRounds()
        {
            //---!!!--- check this!!!
			
			var sortedRoundVMList = from item in ContainedRoundVMs
                           orderby item.RoundModel.Roundordernr
                           select item;
						   
			ContainedRoundVMs = sortedRoundVMList.ToList();
        }

        
    }
}
