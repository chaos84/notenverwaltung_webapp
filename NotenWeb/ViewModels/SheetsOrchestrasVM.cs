﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace NotenWeb.ViewModels
{

    /// <summary>
    /// SheetsOrchestrasVM as ViewModel for Searching and Selecting Orchestras
    /// </summary>
    /// 
    public class SheetsOrchestrasVM
    {

        #region Properties

        public List<SheetVM> SheetVMs { get; set; }

        //Note: SelectLists compare for strings.
        //Options Archive(string-value 0) and All(string value -1) are added in view.

        public SelectList SelListOrchestras { get; set; }
		
		// used to hide links of not PowerUser or Admin in IndexSheets. Hide "create"
        // Note: In Details (Edit Link) WriteAccess of SheetVM is used
		public bool WriteAccess { get; set; }
        
		public string SelOrchestra { get; set; }
        public string SearchString { get; set; }


        #endregion


    }
}
