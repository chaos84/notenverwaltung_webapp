﻿using NotenWeb.Models;
using System.Collections.Generic;
using System.Diagnostics;

namespace NotenWeb.ViewModels
{

    /// <summary>
    /// Round VM. Contains a RoundModel and a List of ContainedSheets.
    /// They are parsed from the SheetIdString in RoundModel. This is done with GenerateSheetsList()
	/// in the setter of the RoundModel
    /// </summary>
    /// 
    public class RoundVM
    {

        #region Properties

        private Round _roundModel;
        public Round RoundModel
        {
            get { return _roundModel; }
            set
            {
                _roundModel = value;
                if((AllSheets != null) & (value != null))
                {
                    GenerateSheetsList();
                }
            }
        }
		

        public List<Sheet> ContainedSheets { get; set; }

        #endregion

        #region Fields

        List<Sheet> AllSheets = null;

        #endregion

		/// <summary>
		/// Constructor for RoundVM, which needs a RoundModel and a List of SheetModels
		/// from which the Contained List is extracted
		/// </summary>
		/// 
        public RoundVM(Round r, ref List<Sheet> reflsm)
        {
            AllSheets = reflsm; // do this at first, otherwise you can't set Round
            RoundModel = r; // this calls "GenerateSheetsList"
        }

		
		
		/// <summary>
		/// Parse SheetIDList string from RoundModel and Generate List of SheetModels
		/// </summary>
		/// 
        private void GenerateSheetsList()
        {
            bool success = false;
            ContainedSheets = new List<Sheet>();

            string[] subStrings = RoundModel.Sheetidlist.Split(';');

            foreach (string str in subStrings)
            {
                success = int.TryParse(str, out int SheetId);
                if (success)
                {
                    bool found = false;
                    for (int n = 0; n < AllSheets.Count; n++)
                    {
                        if (AllSheets[n].Id == SheetId)
                        {
                            Debug.WriteLine("Set RoundSheet " + AllSheets[n].Id + " - " + AllSheets[n].Title);
                            found = true;
                            ContainedSheets.Add(AllSheets[n]);
                            break;
                        }
                    }
                    if(!found)
                    {
                        Debug.WriteLine("RoundSheet not found " + SheetId + " - Adding dummy");
                        Sheet DummySM = new Sheet();
                        DummySM.Title = "--FEHLER-- Stück nicht gefunden";
                        DummySM.Id = SheetId;
                        ContainedSheets.Add(DummySM);
                    }
                }
                // not necessary
                //else
                //{
                //    //Error in Parse from SheetIDString
                //    Debug.WriteLine("Error in Parse from SheetIDString");
                //}

            }
        }




    }
}
