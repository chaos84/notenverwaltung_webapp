﻿using Microsoft.AspNetCore.Identity;
using NotenWeb.Models;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace NotenWeb.ViewModels
{

    /// <summary>
    /// User VM for Role: Admin, PowerUser or ReadOnlyUser.
    /// All other Properties (UserName, AccessFailedCount, LockoutEnd) 
	/// are in the underlying AppUsers
    /// </summary>
    /// 
    public class UserVM
    {
			
        #region Properties
        public AppUsers UserModel { get; set; }

        [Display(Name = "Admin")]
        public bool IsAdmin { get; set; }
		
		[Display(Name = "PowerUser")]
        public bool IsPU { get; set; }
		
		[Display(Name = "ReadOnly")]
        public bool IsRO { get; set; }


		[Display(Name = "Passwort")]
		//[ValidatePasswordLength]
		[DataType(DataType.Password)]
		[Required(ErrorMessage = "Password darf nicht leer sein")]
        public string password { get; set; }
        #endregion


        /// <summary>
        /// Constructor of UserVM, which uses AppUsers (UserModel) and UserManager (for Roles) as parameter.
        /// Constructor which is parameterless is necessary. This is called, when clicking on Create-Button
        /// </summary>
        /// <param name="user">the underlying UserModel</param>
        /// <param name="userManager">Dependency Injected User Manager for Roles</param>

        // gets called when clicking on Create-Button in Create View

        public UserVM()
        {
            UserModel = new AppUsers();
            IsAdmin = false;
			IsPU = false;
			IsRO = false;
        }


        public UserVM(AppUsers user, UserManager<AppUsers> userManager)
        {
            UserModel = user;
			
			IsAdmin = false;
			IsPU = false;
			IsRO = false;

            // Set Bools according to Roles
			// Call this synchronously, because no async constructor.
			// Gives Back a string [] of role-names, in which the user is member
            var roles = Task.Run(() => userManager.GetRolesAsync(UserModel)).Result;
                
			foreach (string role in roles)
			{
				if (role.Contains("Admin"))
					IsAdmin = true;
				
				if (role.Contains("PowerUser"))
					IsPU = true;
			
				if (role.Contains("ReadOnlyUser"))
					IsRO = true;
			}

        }


    }
}
