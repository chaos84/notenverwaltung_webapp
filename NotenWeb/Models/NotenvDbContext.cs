﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace NotenWeb.Models
{
    /// <summary>
    /// The database representational model for notenverwaltung Web.
    /// IdentityDbContext adds a bunch of DbSets for managing users, authentication usw. 
     /// </summary>
    public partial class NotenvDbContext : IdentityDbContext<AppUsers>
    {

        /// <summary>
        /// Constructor expected Database Options passed in. No Empty constructor.
        /// </summary>
        public NotenvDbContext(DbContextOptions<NotenvDbContext> options)
            : base(options)
        {
        }


        #region Properties

        // DbSet is like a Table
        // these get checked by EnsureCreated
        public virtual DbSet<AppUsers> AppUsers { get; set; }
        public virtual DbSet<Orchestra> Orchestra { get; set; }
        public virtual DbSet<Sheet> Sheet { get; set; }
        public virtual DbSet<Round> Round { get; set; }

        #endregion




        // this is not necessary, as the DbContext is dependency injected

        //        /// <summary>
        //        /// Gets fired, when the Database shall be accessed, e.g. EnsureCreated
        //        /// </summary>
        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //        {
        //            if (!optionsBuilder.IsConfigured)
        //            {
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
        //                optionsBuilder.UseMySQL("server=sjk.diskstation.me;port=3307;user=doris;password=doris1!;database=notenverwaltung;SslMode=Preferred;");
        //            }
        //        }




        /// <summary>
        /// Gets fired, when the Tables shall be created.
        /// </summary>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            // as alternative to the [ ] tags in the Models, this can be done here and is called Fluent API

            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");


            modelBuilder.Entity<Orchestra>(entity =>
            {
                entity.ToTable("orchestra", "notenverwaltung");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.OrderNr)
                    .HasColumnName("order_nr")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Label)
                    .HasColumnName("label")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.UsesAlphabetically)
                    .HasColumnName("uses_alphabetically")
                    .HasColumnType("BOOL") // this was before (auto scaffold) tinyint(1). now bool. seems better.
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.UsesInventoryNumber)
                    .HasColumnName("uses_inventory_number")
                    .HasColumnType("BOOL") // this was before (auto scaffold) tinyint(1). now bool. seems better.
                    .HasDefaultValueSql("NULL");
            });

            modelBuilder.Entity<Round>(entity =>
            {
                entity.ToTable("round", "notenverwaltung");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Label)
                    .HasColumnName("label")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Roundgroup)
                    .HasColumnName("roundgroup")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Roundordernr)
                    .HasColumnName("roundordernr")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Sheetidlist)
                    .HasColumnName("sheetidlist")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");
            });

            modelBuilder.Entity<Sheet>(entity =>
            {
                entity.ToTable("sheet", "notenverwaltung");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Arrangeur)
                    .HasColumnName("arrangeur")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Composer)
                    .HasColumnName("composer")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Difficulty)
                    .HasColumnName("difficulty")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Duration)
                    .HasColumnName("duration")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.InventoryNumber)
                    .HasColumnName("inventory_number")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.LendTo)
                    .HasColumnName("lend_to")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.OrchestraId)
                    .HasColumnName("orchestra_id")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Score)
                    .HasColumnName("score")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");
            });
        }

    }
}
