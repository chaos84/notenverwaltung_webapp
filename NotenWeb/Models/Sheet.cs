﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NotenWeb.Models
{
    /// <summary>
    /// Sheet Model. Price, Duration and Orchestra-Infos are converted in the SheetVM!
    /// Therefore here no Properties!
    /// </summary>
    public class Sheet
    {
        // Attributes are in fluent API in NotenvDbContext: MaxLength
        public int Id { get; set; }

        // Converted in SheetVM
        public int? OrchestraId { get; set; }

        [Display(Name = "Nr")]
        [Range(1,100000,
            ErrorMessage = "Inventarnummer muss zwischen 1 und 100.000 liegen.")]
        public int? InventoryNumber { get; set; }

        [Display(Name = "Titel")]
        [RegularExpression(@"[A-Za-z0-9.,:_#'~+\-\*äöüÄÖÜ<>|^°!""§$%&/()=?{[\]}\\#\s]*", 
            ErrorMessage = "Kein Semikolon (Strichpunkt) erlaubt")]
        public string Title { get; set; }

        [Display(Name = "Komponist")]
        [RegularExpression(@"[A-Za-z0-9.,:_#'~+\-\*äöüÄÖÜ<>|^°!""§$%&/()=?{[\]}\\#\s]*",
            ErrorMessage = "Kein Semikolon (Strichpunkt) erlaubt")]
        public string Composer { get; set; }

        //Composer is not used, Arrangeur as combi-field for Arrangeur / Composer
        [Display(Name = "Arrangeur / Komponist")]
        [RegularExpression(@"[A-Za-z0-9.,:_#'~+\-\*äöüÄÖÜ<>|^°!""§$%&/()=?{[\]}\\#\s]*",
            ErrorMessage = "Kein Semikolon (Strichpunkt) erlaubt")]
        public string Arrangeur { get; set; }

        [Display(Name = "Schwierigkeit")]
        public int? Difficulty { get; set; }

        [Display(Name = "Partitur")]
        [RegularExpression(@"[A-Za-z0-9.,:_#'~+\-\*äöüÄÖÜ<>|^°!""§$%&/()=?{[\]}\\#\s]*",
            ErrorMessage = "Kein Semikolon (Strichpunkt) erlaubt")]
        public string Score { get; set; }

        // Converted in SheetVM
        public int? Duration { get; set; }

        // Converted in SheetVM
        public int? Price { get; set; }

        [Display(Name = "Ausgeliehen")]
        [RegularExpression(@"[A-Za-z0-9.,:_#'~+\-\*äöüÄÖÜ<>|^°!""§$%&/()=?{[\]}\\#\s]*",
            ErrorMessage = "Kein Semikolon (Strichpunkt) erlaubt")]
        public string LendTo { get; set; }

        [Display(Name = "Notizen")]
        [RegularExpression(@"[A-Za-z0-9.,:_#'~+\-\*äöüÄÖÜ<>|^°!""§$%&/()=?{[\]}\\#\s]*",
            ErrorMessage = "Kein Semikolon (Strichpunkt) erlaubt")]
        public string Notes { get; set; }
    }
}
