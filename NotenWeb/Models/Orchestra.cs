﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NotenWeb.Models
{
    public partial class Orchestra
    {
        // Attributes are in fluent API in NotenvDbContext (Maxlenght)
        public int Id { get; set; }

        // OrderNumber not null allowed, so 0 as default
        public int OrderNr { get; set; } = 0;

        [Display(Name = "Name")]
		public string Label { get; set; }

        [Display(Name = "nach Nummer?")]
        public bool? UsesInventoryNumber { get; set; } // before: byte as datatype

        [Display(Name = "alphabetisch?")]
        public bool? UsesAlphabetically { get; set; } // before: byte as datatype



        // converting bool? to bool
        [NotMapped]
        public bool UsesInventoryNumberX
        {
            get { return UsesInventoryNumber ?? false; }
            set { UsesInventoryNumber = value; }
        }

        [NotMapped]
        public bool UsesAlphabeticallyX
        {
            get { return UsesAlphabetically ?? false; }
            set { UsesAlphabetically = value; }
        }
    }
}
