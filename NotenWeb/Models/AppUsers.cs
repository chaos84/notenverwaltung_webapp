﻿using Microsoft.AspNetCore.Identity;

namespace NotenWeb.Models
{
    /// <summary>
    /// User Data & Profile from Built In IdentityUser Class. This has several own rows
    /// </summary>
    public class AppUsers : IdentityUser
    {
        // no own tables necessary
    }
}
