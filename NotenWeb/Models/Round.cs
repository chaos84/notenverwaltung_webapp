﻿namespace NotenWeb.Models
{
    public class Round
    {
        // Attributes are in fluent API in NotenvDbContext
        public int Id { get; set; }
        public int? Roundordernr { get; set; }
        public string Label { get; set; }
        public string Roundgroup { get; set; }
        public string Sheetidlist { get; set; }
    }
}
