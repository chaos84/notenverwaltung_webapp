﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace NotenWeb
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {

            // WebHost.CreateDefaultBuilder runs a bunch of Startup Code Most common Setup Environment:
            // -UseKestrel
            // -UseContentRoot (where the .csproj File is)
            // -ConfigureAppConfigureation (this reads appsettings.json. Sub-File when "Development" is set for more outputs. Add these files if found)
            // -If Environment == IsDevelopment, Add UserSecrets. AngelSix sagt, das ist ziemlich sinnlos und machts nicht sicherer.
            // -Add Environment Variables. This adds current environment variables of machine to environment provider.
            // -Add CommandLine-Arguments
            // -Configure Logging; sucht nach hostingContext, GetSection, Logging --> Endet in appsettings.
            // -- Add Debug und Add Console
            // -Adds ValidationScopes


            return WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>() // tells the WebServer what to do when Start - Class Startup
                .Build();


            //This runs only absolutely bare minimum stuff without appsettings, Logging and so far
            //return new WebHostBuilder()
            //    .UseKestrel()
            //    .UseContentRoot(Directory.GetCurrentDirectory())
            //    .UseStartup<Startup>() // tells the WebServer what to do when Start - Class Startup
            //    .Build();
        }

    }
}
