﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NotenWeb.Models;
using System;

namespace NotenWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            // this gives the configuration (Logging, Environment...) from Program.cs via ConfigurationProviders
            // "Startup gets dependency injected with configuration"
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }



        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        public void ConfigureServices(IServiceCollection services)
        {

            // inject DbContext. on every controller construction. means on every page-request
            services.AddDbContext<NotenvDbContext>(options =>
                options.UseMySql(Configuration.GetConnectionString("DefaultConnection")) // in appsettings
            );

            // AddIdentity adds Cookie based authentication,
            // adds scoped classes for things like UserManager oder SigninManager, PasswordHashes...
            // NOTE: Automatically adds the validated user from a cookie (which is before on the user machine) to the HttpContext.User
            // which can be used in teh controllers
            // https://github.com/aspnet/Identity/blob/master/src/Identity/IdentityServiceCollectionExtensions.cs
            services.AddIdentity<AppUsers, IdentityRole>()

                    // Adds UserStore and RoleStore from this context.
                    // that are consumed by the UserManager and RoleManager
                    .AddEntityFrameworkStores<NotenvDbContext>()

                    // Adds a provider that generates unique keys and hashes
                    // for things like forgot password links, phone number verfication...
                    .AddDefaultTokenProviders();

            // Password Policy 
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 5;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = false;
            });


            //Change Login URL is not necessary, because default: /Account/Login
            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/Account/IndexLogin";


                // Change Cookie Timeout
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);

            });

            


            // inject MVC style to the whole server
            services.AddMvc();
        }



        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// Added serviceProvider 
        /// </summary>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            // Setup Identity
            // The above Service is used (Identity Authentication)
            app.UseAuthentication();


            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                // this shows an html-page with exception and kind of trace to developer
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // in production this tries to find a page called /Home/Error.cshtml 
                app.UseExceptionHandler("/Home/Error");
            }


            // this allows requests to files. e.g. /images/bild1.jpg
            app.UseStaticFiles();


            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                //id ist eine Variable oder Parameter. Man könnte im Controller einen String definieren, der das enthält
                // der name muss dann string id = "" sein.

                // wenn ich z.b. mit /more auf die Seite about/tellmemore(/Index.cshtml) kommen will:

                //routes.MapRoute(
                //    name: "aboutPage",
                //    template: "more",
                //    defaults: new { controller = "About", action = "TellMeMore" });
            });
        }
    }
}
