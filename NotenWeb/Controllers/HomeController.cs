﻿using Microsoft.AspNetCore.Mvc;

namespace NotenWeb.Controllers
{

    /// <summary>
    /// This is related to FolderNames in View! - It searches for "Home" because of "HomeController"
    /// Every single call to the web-page constructs a new controller.
    /// </summary>
    public class HomeController : Controller
    {

        /// <summary>
        ///  Constructor of Home Controller
        /// </summary>
        public HomeController()
        {
        }

        /// <summary>
        /// GET /Home/Index (or just empty because of default route)
        /// This MAIN ENTRY POINT links to IndexSheets. 
        /// If not logged in, the default login path goes to Account/Login, as IndexSheets is [Authorize]
        /// </summary>
        /// <returns>Redirect to /IndexSheets/Sheets</returns>
        public IActionResult Index()
        {
            // This means: Go and find me a file in the Folder View inside Home called Index.cshtml
            return RedirectToAction("IndexSheets", "Sheets");
        }




        /// <summary>
        /// GET /Home/Error
        /// Error Page when Database Error Occurs. This means tables not reachable or not created.
        /// </summary>
        /// <returns></returns>
        public IActionResult Error()
        {
            return View();
        }

    }
}
