﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NotenWeb.Models;
using NotenWeb.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;


/// <summary>
/// Rounds Controller for IndexRounds
/// Note: no CRUD. No edit supported in WebApp
/// </summary>

namespace NotenWeb.Controllers
{
    public class RoundsController : Controller
    {
        #region Fields
        protected NotenvDbContext _context;

        #endregion


        /// <summary>
        /// Constructor of Rounds Controller, which also gets new DbContext
        /// this is called the constructor injector and this works, because 
        /// NotenvDbContext is added as service in startup - configure  service
        /// 
        /// Note: Sheets use a ViewModel because of Price, Duration and WriteAccess. 
        /// And the IndexSheets uses another VM for search.
        /// Rounds use also VM because RoundGroups
        /// </summary>
        /// <param name="context">Database Context</param>

        public RoundsController(NotenvDbContext context)
        {
            _context = context;
        }



        /// <summary>
        /// GET /Rounds/IndexRounds
        /// main page of Rounds with table. 
        /// Only for Authorized Users
        /// </summary>

        [Authorize(Roles = "Admins,ReadOnlyUser,PowerUser")]
        public async Task<IActionResult> IndexRounds()
        {
			// Note: Try to use Sheets without SheetVM, because they also need Orchestras.
			// Orchestra, Price, Duration,... doesn't matter here.
			
			List<Sheet> SheetModelList = await _context.Sheet.ToListAsync();
			List<Round> RoundModelList = await _context.Round.ToListAsync();
			
			List<RoundGroupVM> RoundGroupVMs = new List<RoundGroupVM>();
			
			for(int n = 0; n < RoundModelList.Count; n++)
            {
                RoundVM rvm = new RoundVM(RoundModelList[n], ref SheetModelList);

                if (n==0) //first element: Make first Roundgroup
                {
                    RoundGroupVMs.Add(new RoundGroupVM(rvm.RoundModel.Roundgroup)); //generate roundgroup with first label and add it to collection
                }

                bool RoundGroupAlreadyExists = false;
                foreach (RoundGroupVM rgvm in RoundGroupVMs)
                {
                    if(rvm.RoundModel.Roundgroup == rgvm.RoundGroupLabel)
                    {
                        rgvm.AddRound(rvm);
                        RoundGroupAlreadyExists = true;
                    }
                }

                if (!RoundGroupAlreadyExists)
                {
                    RoundGroupVM rgvm = new RoundGroupVM(rvm.RoundModel.Roundgroup);
                    rgvm.AddRound(rvm);
                    
                    RoundGroupVMs.Add(rgvm);
                }
            }

            //sort all
            foreach(RoundGroupVM rgvm in RoundGroupVMs)
            {
                rgvm.SortRounds();
            }
			
			
			
			
			
			return View(RoundGroupVMs);
        }




    }
}

