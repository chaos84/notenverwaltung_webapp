﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NotenWeb.Models;
using NotenWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


/// <summary>
/// Sheets Controller for IndexSheets and CRUD operations
/// </summary>

namespace NotenWeb.Controllers
{
    public class SheetsController : Controller
    {
        #region Fields
        protected NotenvDbContext _context;

        // used to generate the VM of the Sheets with orchestra names
        private List<Orchestra> _listOrchestras;

        #endregion


        /// <summary>
        /// Constructor of Sheets Controller, which also gets new DbContext
        /// this is called the constructor injector and this works, because 
        /// NotenvDbContext is added as service in startup - configure  service
        /// </summary>
        /// <param name="context">Database Context</param>

        public SheetsController(NotenvDbContext context)
        {
            _context = context;
            _listOrchestras = _context.Orchestra.OrderBy(o => o.OrderNr).ToList();
        }



        /// <summary>
        /// GET /Sheets/IndexSheets/(optional: ?SearchString=Tequila )
        /// Note: ?SearchString=Tequila can be appended and is bound to parameter string SearchString. 
        /// 
        /// Note: SearchString and SelOrchestra are submitted by asp-for and method = get. This makes
        /// - binding to VM (asp-for)
        /// - url-parameter (get)
        /// without asp-for and just "name"-tag the url-parameter stays, but the searchstring is not retained at page
        /// 
        /// main page of sheets with table. 
        /// Only for Authorized Users
        /// </summary>

        [Authorize(Roles = "Admins,ReadOnlyUser,PowerUser")]
        public async Task<IActionResult> IndexSheets(string SelOrchestra, string SearchString)
        {
            // Note: Query is only defined. Nothing is done in database yet.
            // No Sheets from recycler
            var sheets = _context.Sheet.Where(s => s.OrchestraId != -2)
                                       .OrderBy(s => s.OrchestraId).ThenBy(s => s.InventoryNumber)
                                       .Select(s => s);



            // Get List of Orchestras
            var orchestras = from o in _context.Orchestra
                        orderby o.OrderNr
						select o;

						
			// Check if user has write-access
			bool write = false;
			if (User.IsInRole("Admins") | User.IsInRole("PowerUser") )
				write = true;
			
	
	
			// Filter sheets for SearchString (no SheetVM necessary)
            if (!string.IsNullOrEmpty(SearchString))
            {
                // Note: query execution is deferred. 
                // That means that the evaluation of an expression is delayed 
                // until its realized value is actually iterated over or the 
                // ToListAsync method is called.
                sheets = sheets.Where(s => s.Title.Contains(SearchString));
            }

            // Filter sheets for Orchestra. 
            // All: string=-1. Archive: string="0". options added in View. 
            // Other Orchestras compared with string-values. ; and & are ok

			if (!string.IsNullOrEmpty(SelOrchestra))
			{
				// find Orchestra ID of selected orchestra
				int SelID = -1;
				if(SelOrchestra == "0")
					SelID = 0;
				
				foreach (Orchestra o in orchestras)
				{
					if (o.Label == SelOrchestra)
					{
						SelID = o.Id;
						break;
					}
				}
				
				// only when Orchestra Filter selected
				if ( SelID > -1)
					sheets = sheets.Where(s => s.OrchestraId == SelID); 
				
			}
			
			
            // executes query on database
            var SheetModelList = await sheets.ToListAsync();


            // Build List of SheetVM here in the controller
            List<SheetVM> ControllerSheetVMs = new List<SheetVM>();

            foreach (Sheet SheetModel in SheetModelList)
            {
                SheetVM svm = new SheetVM(SheetModel, ref _listOrchestras);
                ControllerSheetVMs.Add(svm);
            }

            // Build SheetsOrchestrasVM;
            // use LINQ to get list of Orchestras
            //Note: SelectLists compare for strings.
            //Options Archive(string-value 0) and All(string value -1) are added in view.

            IQueryable<string> orchQuery = from o in orchestras
                                            select o.Label;

            var sovm = new SheetsOrchestrasVM
			{
				SheetVMs = ControllerSheetVMs,
				SelListOrchestras = new SelectList(orchQuery), // Archive and All added in View
				WriteAccess = write // hiding links in IndexSheets
			};
            
            return View(sovm);
        }



        /// <summary>
        /// GET: /Sheets/Details/5
        /// Only for Authorized Users
        /// As Details shows price and duration, these have to be converted (int in database)
        /// therefore SheetVM is generated
        /// Shows Details for one sheet
        /// </summary>
        /// <param name="id">Sheet Id for which details shall be shown</param>
        /// <returns></returns>

        [Authorize(Roles = "Admins,ReadOnlyUser,PowerUser")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sheet = await _context.Sheet
                .SingleOrDefaultAsync(m => m.Id == id);
            if (sheet == null)
            {
                return NotFound();
            }

			// Check if user has write-access
			bool write = false;
			if (User.IsInRole("Admins") | User.IsInRole("PowerUser") )
				write = true;
			
			
            // convert price and duration, generate VM
            SheetVM svm = new SheetVM(sheet, ref _listOrchestras);
			svm.WriteAccess = write; // hiding "Edit"

            return View(svm);
        }



        /// <summary>
        /// GET: /Sheets/Create
        /// Empty SheetVM can't be created automatically, because list of orchestras have to be generated
        /// Note: Write-Access in SheetVM = false, but this is not considered
        /// 
        /// Only for Authorized Users (Admins and PowerUsers)
        /// </summary>

        [Authorize(Roles = "Admins,PowerUser")]
        public IActionResult Create()
        {

            // Empty, only list of Orchestras for SelectList
            SheetVM svm = new SheetVM(ref _listOrchestras);
            
            return View(svm);
        }




        /// <summary>
        /// POST /Sheets/Create 
        /// can be invoked only for POST requests
        /// converts SheetViewModel to sheet (Price and Duration)
        /// tries to create a new sheet --> This calls parameterless SheetVM constructor
        /// </summary>
        /// <param name="sheetVM"></param>
        /// <returns></returns>
        
        [Authorize(Roles = "Admins,PowerUser")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            //[Bind("SheetModel,OrchestraName,Duration,Price")] // Binding Property not necessary as no overposting possible
            // This means no secret property. And Id is checked.
            SheetVM sheetVM)
        {
            if (ModelState.IsValid)
            {
                // convert sheetVM back to sheet (price, duration, OrchestraId)

                bool found = sheetVM.ConvertBack(ref _listOrchestras);

                //----------!!!----------- DEBUG
                if (!found)
                    Console.WriteLine("\n----- Orchestra string was not found. SheetsController -> ConvertBack");

                _context.Add(sheetVM.SheetModel);
                await _context.SaveChangesAsync();
                return RedirectToAction("IndexSheets");
            }
            
            // if model is not valid / validation fails: show same page
            return View(sheetVM);
        }

        

        /// <summary>
        /// GET: /Sheets/Edit/5
        /// Only for Authorized Users (Admins and PowerUsers)
        /// As Edit shows price and duration, these have to be converted (int in database)
        /// Shows Edit for one sheet
        /// </summary>
        /// <param name="id">Sheet Id for the Sheet to be Edited</param>
        /// <returns></returns>

        [Authorize(Roles = "Admins,PowerUser")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sheet = await _context.Sheet.SingleOrDefaultAsync(m => m.Id == id);
            if (sheet == null)
            {
                return NotFound();
            }

            // convert price and duration, generate VM
            SheetVM svm = new SheetVM(sheet, ref _listOrchestras);

            return View(svm);
        }



        /// <summary>
        /// POST: /Sheets/Edit/5
        /// Only for Authorized Users (Admins and PowerUsers)
        /// As Edit shows price and duration, these have to be converted (int in database)
        /// Saves the edited sheet
        /// </summary>
        /// <param name="id">Sheet Id for the Sheet to be Edited</param>
        /// <param name="sheetVM">New Sheet Information</param>
        /// <returns></returns>

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        [Authorize(Roles = "Admins,PowerUser")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            //[Bind("SheetModel,OrchestraName,Duration,Price")] // Binding Property not necessary as no overposting possible
                                                                // This means no secret property. And Id is checked.
            SheetVM sheetVM)
        {
            if (id != sheetVM.SheetModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    bool found = sheetVM.ConvertBack(ref _listOrchestras);

                    //----------!!!----------- DEBUG
                    if (!found)
                        Console.WriteLine("\n----- Orchestra string was not found. SheetsController -> ConvertBack");

                    _context.Update(sheetVM.SheetModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    bool exists = await SheetExistsAsync(sheetVM.SheetModel.Id);
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("IndexSheets");
            }
            return View(sheetVM);
        }




        /// <summary>
        /// GET: /Sheets/Delete/5
        /// Only for Authorized Users (Admins and PowerUsers)
        /// Show sheet which shall be deleted
        /// </summary>
        /// <param name="id">Sheet Id for the Sheet to be Deleted</param>

        [Authorize(Roles = "Admins,PowerUser")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sheet = await _context.Sheet
                .SingleOrDefaultAsync(m => m.Id == id);
            if (sheet == null)
            {
                return NotFound();
            }

            // convert price and duration, generate VM
            SheetVM svm = new SheetVM(sheet, ref _listOrchestras);

            return View(svm);
        }



        /// POST: /Sheets/DeleteConfirmed/5
        /// Only for Authorized Users (Admins and PowerUsers)
        /// Deletes the sheet
        /// Note: ActionName("Delete") makes this action accessible in the Delete Form
        /// with id
        /// </summary>
        /// <param name="id">Sheet Id for the Sheet to be Deleted</param>
        /// <returns></returns>

        [HttpPost]
        [ActionName("Delete")]
        [Authorize(Roles = "Admins,PowerUser")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var sheet = await _context.Sheet.SingleOrDefaultAsync(m => m.Id == id);
            _context.Sheet.Remove(sheet);
            await _context.SaveChangesAsync();
            return RedirectToAction("IndexSheets");
        }



        /// <summary>
        /// This checks asynchronously whether a sheet with parameter id exists
        /// </summary>
        /// <param name="id">Sheet id to be checked for exist</param>
        /// <returns>true, if sheet exists</returns>

        private async Task<bool> SheetExistsAsync(int id)
        {
            bool result = await _context.Sheet.AnyAsync(e => e.Id == id);
            return result;
        }




    }
}

