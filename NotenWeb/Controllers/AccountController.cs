﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NotenWeb.Models;

namespace NotenWeb.Controllers
{
    /// <summary>
    /// Account Controller for Login-Page, DoLoginAsync, DoLogoutAsync
    /// </summary>

    public class AccountController : Controller
    {

        #region Fields
        protected NotenvDbContext _context;

        // Manager for User Creation, Deletion ...
        protected UserManager<AppUsers> _userManager;

        // Manager for sign in und out for our users
        protected SignInManager<AppUsers> _signinManager;

        // Manager for roles
        protected RoleManager<IdentityRole> _roleManager;


        protected string _returnURL;

        #endregion


        /// <summary>
        /// Constructor of Account Controller, which also gets new DbContext
        /// this is called the constructor injector and this works, because NotenvDbContext is added as service in startup - configure  service
        /// </summary>
        /// <param name="context">Database Context</param>
        /// <param name="userManager">User Manager</param>
        /// <param name="signinManager">Signin Manager</param>
        /// <param name="roleManager">Role Manager</param>

        public AccountController(
            NotenvDbContext context,
            UserManager<AppUsers> userManager,
            SignInManager<AppUsers> signinManager,
            RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _signinManager = signinManager;
            _roleManager = roleManager;
            _returnURL = null;
        }



        /// <summary>
        /// GET /Account/IndexLogin (default entry for accessing [Authorized] stuff)
        /// If the user is logged in, it redirects to /Home/Index --> /Sheets/IndexSheets
        /// 
        /// It checks Database Access and Tables. If not OK, it redirects to /Home/Error
        /// </summary>
        /// <param name="returnUrl">Return Url where the user came from</param>
        /// <returns></returns>

        public async Task<IActionResult> IndexLogin(string returnUrl)
        {
            // if user is already signed in, go to sheets, because layout for signed in users is
            // not fitting to login page.
            if(User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");


            // store return URL for after login
            if (!string.IsNullOrEmpty(returnUrl))
                _returnURL = returnUrl;

            // Make sure that database exists.
            // If Database exists, no effort is made to check tables
            _context.Database.EnsureCreated();

            // check tables manually by trying to access
            try
            {
                int number = await _context.Set<AppUsers>().CountAsync();
                number = await _context.Set<Sheet>().CountAsync();
                number = await _context.Set<Orchestra>().CountAsync();
                number = await _context.Set<Round>().CountAsync();
                //number = await _context.Set<RouteAttribute>().CountAsync(); // use this line to simulate an error
            }
            catch
            {
                // Go To Error-Page, which shows database-error
                return RedirectToAction("Error", "Home");
            }

            
            return View();
        }




        /// <summary>
        /// POST /Account/DoLoginAsync 
        /// can be invoked only for POST requests
        /// tries to signin the user
        /// 
        /// </summary>
        /// <param name="username">The Username from the form</param>
        /// <param name="pass">The Password from the form</param>
        /// <returns>redirect to returnUrl or /Home/Index (which then --> Sheets/IndexSheets)</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DoLoginAsync(string username, string pass)
        {
            if( string.IsNullOrWhiteSpace(username) | string.IsNullOrWhiteSpace(pass) )
                return RedirectToAction("Index", "Home");

            var result = await _signinManager.PasswordSignInAsync(username, pass, true, true); 
            // last value: lockout on failure, set to true.
            // it uses default options: 5 attempts, then 5 minutes logout.


            if (string.IsNullOrEmpty(_returnURL))
                return RedirectToAction("Index", "Home"); //this is redirected to /Sheets/IndexSheets
            else
                return Redirect(_returnURL);
        }

        [HttpPost]
        public async Task<IActionResult> DoLogoutAsync()
        {
            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);
            return RedirectToAction("Index", "Home");
        }







    }


    

}