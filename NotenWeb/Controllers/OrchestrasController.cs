﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NotenWeb.Models;
using System.Linq;
using System.Threading.Tasks;


/// <summary>
/// Orchestras Controller for IndexOrchestras and CRUD operations
/// </summary>

namespace NotenWeb.Controllers
{
    public class OrchestrasController : Controller
    {
        #region Fields
        protected NotenvDbContext _context;

        #endregion


        /// <summary>
        /// Constructor of Orchestras Controller, which also gets new DbContext
        /// this is called the constructor injector and this works, because 
        /// NotenvDbContext is added as service in startup - configure  service
        /// 
        /// Note: Sheets use a ViewModel because of Price, Duration and WriteAccess. 
        /// And the IndexSheets uses another VM for search.
        /// Orchestras use directly the Model without VM
        /// </summary>
        /// <param name="context">Database Context</param>

        public OrchestrasController(NotenvDbContext context)
        {
            _context = context;
        }



        /// <summary>
        /// GET /Orchestras/IndexOrchestras
        /// main page of orchestras with table. 
        /// Only for Authorized Users
        /// </summary>

        [Authorize(Roles = "Admins,ReadOnlyUser,PowerUser")]
        public async Task<IActionResult> IndexOrchestras()
        {
            // Note: Query is only defined. Nothing is done in database yet.
            // Get List of Orchestras
            var orchestras = from o in _context.Orchestra
                             orderby o.OrderNr
                             select o;

            return View(await orchestras.ToListAsync());
        }



        



        /// <summary>
        /// GET: /Orchestras/Create
        /// Only for Authorized Users (Admins and PowerUsers)
        /// </summary>

        [Authorize(Roles = "Admins,PowerUser")]
        public IActionResult Create()
        {
            return View();
        }





        /// <summary>
        /// POST /Orchestras/Create 
        /// can be invoked only for POST requests
        /// tries to create a new Orchestra
        /// </summary>
        /// <param name="orchestraVM"></param>
        /// <returns></returns>
        
        [Authorize(Roles = "Admins,PowerUser")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            //[Bind("SheetModel,OrchestraName,Duration,Price")] // Binding Property not necessary as no overposting possible
            // This means no secret property. And Id is checked.
            Orchestra orchestra)
        {
            if (ModelState.IsValid)
            {
                _context.Add(orchestra);
                await _context.SaveChangesAsync();
                return RedirectToAction("IndexOrchestras");
            }
            
            // if model is not valid / validation fails: show same page
            return View(orchestra);
        }

        

        /// <summary>
        /// GET: /Orchestras/Edit/5
        /// Only for Authorized Users (Admins and PowerUsers)
        /// Shows Edit for one orchestra
        /// </summary>
        /// <param name="id">Orchestra Id for the Orchestra to be Edited</param>
        /// <returns></returns>

        [Authorize(Roles = "Admins,PowerUser")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orchestra = await _context.Orchestra.SingleOrDefaultAsync(o => o.Id == id);
            if (orchestra == null)
            {
                return NotFound();
            }

            return View(orchestra);
        }



        /// <summary>
        /// POST: /Orchestras/Edit/5
        /// Only for Authorized Users (Admins and PowerUsers)
        /// Saves the edited orchestra
        /// </summary>
        /// <param name="id">Orchestra Id for the Orchestra to be Edited</param>
        /// <param name="orchestra">New Orchestra Information</param>
        /// <returns></returns>

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        [Authorize(Roles = "Admins,PowerUser")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            //[Bind("SheetModel,OrchestraName,Duration,Price")] // Binding Property not necessary as no overposting possible
                                                                // This means no secret property. And Id is checked.
            Orchestra orchestra)
        {
            if (id != orchestra.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(orchestra);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    bool exists = await OrchestraExistsAsync(orchestra.Id);
                    if (!exists)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("IndexOrchestras");
            }
            return View(orchestra);
        }




        /// <summary>
        /// GET: /Orchestras/Delete/5
        /// Only for Authorized Users (Admins and PowerUsers)
        /// Show orchestra which shall be deleted
        /// </summary>
        /// <param name="id">orchestra Id for the orchestra to be Deleted</param>

        [Authorize(Roles = "Admins,PowerUser")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var orchestra = await _context.Orchestra
                .SingleOrDefaultAsync(m => m.Id == id);
            if (orchestra == null)
            {
                return NotFound();
            }

            return View(orchestra);
        }



        /// POST: /Orchestras/DeleteConfirmed/5
        /// Only for Authorized Users (Admins and PowerUsers)
        /// Deletes the Orchestra
        /// Note: ActionName("Delete") makes this action accessible in the Delete Form
        /// with id
        /// </summary>
        /// <param name="id">Orchestra Id for the Orchestra to be Deleted</param>
        /// <returns></returns>

        [HttpPost]
        [ActionName("Delete")]
        [Authorize(Roles = "Admins,PowerUser")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var orchestra = await _context.Orchestra.SingleOrDefaultAsync(m => m.Id == id);
            _context.Orchestra.Remove(orchestra);
            await _context.SaveChangesAsync();
            return RedirectToAction("IndexOrchestras");
        }



        /// <summary>
        /// This checks asynchronously whether a sheet with parameter id exists
        /// </summary>
        /// <param name="id">Sheet id to be checked for exist</param>
        /// <returns>true, if sheet exists</returns>

        private async Task<bool> OrchestraExistsAsync(int id)
        {
            bool result = await _context.Orchestra.AnyAsync(e => e.Id == id);
            return result;
        }




    }
}

