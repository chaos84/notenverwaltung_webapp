﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NotenWeb.Models;
using NotenWeb.ViewModels;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;


/// <summary>
/// Users Controller for IndexUsers and Add / Delete (no edit)
/// </summary>

namespace NotenWeb.Controllers
{
    public class UsersController : Controller
    {
        #region Fields
        protected NotenvDbContext _context;

		// Manager for User Creation, Deletion ...
        protected UserManager<AppUsers> _userManager;
		
		// Manager for roles
        protected RoleManager<IdentityRole> _roleManager;
		
        #endregion


        /// <summary>
        /// Constructor of Users Controller, which also gets new DbContext
        /// this is called the constructor injector and this works, because NotenvDbContext is added as service in startup - configure  service
        /// </summary>
        /// <param name="context">Database Context</param>
        /// <param name="userManager">User Manager</param>
        /// <param name="roleManager">Role Manager</param>

		public UsersController(
            NotenvDbContext context,
            UserManager<AppUsers> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }



        /// <summary>
        /// GET /Users/IndexUsers
        /// main page of Users with table. 
		/// Note: Users have a UsersVM, which has bool-variables for the role
        /// Only for Admins
        /// </summary>

        [Authorize(Roles = "Admins")]
        public async Task<IActionResult> IndexUsers()
        {
			// Get List of all Users
			List<AppUsers> AllUsers = await _userManager.Users.ToListAsync();


			// Generate List of UserVMs
			List<UserVM> UserVMs = new List<UserVM>();
			
			foreach (AppUsers user in AllUsers)
			{
				UserVM uVM = new UserVM(user, _userManager);
				UserVMs.Add(uVM);
			}
		
			return View(UserVMs);
        }
		
		
		
		/// <summary>
        /// POST /Users/CreateRoles
        /// can be invoked only for POST requests
        /// tries to create the Roles Admin, PowerUser and ReadOnlyUser (initially necessary and without Admin Deadlock)
        /// </summary>
        /// <returns></returns>
        
        [Authorize(Roles = "Admins")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateRoles()
        {
            string returnstring = "";
            // Create all Roles if not existent
            bool x = await _roleManager.RoleExistsAsync("Admins");
            if (x)
			{
				returnstring += "Rolle Admins existiert bereits \n";
			}
			else
            {
                var role = new IdentityRole();
                role.Name = "Admins";
                await _roleManager.CreateAsync(role);

                returnstring += "Rolle Admins war nicht da, wurde erzeugt \n";
            }

            x = await _roleManager.RoleExistsAsync("ReadOnlyUser");
            if (x)
			{
				returnstring += "Rolle ReadOnlyUser existiert bereits \n";
			}
			else
            {
                var role = new IdentityRole();
                role.Name = "ReadOnlyUser";
                await _roleManager.CreateAsync(role);

                returnstring += "Rolle ReadOnlyUsers war nicht da, wurde erzeugt \n";
            }

            x = await _roleManager.RoleExistsAsync("PowerUser");
            if (x)
			{
				returnstring += "Rolle PowerUser existiert bereits \n";
			}
			else
            {
                var role = new IdentityRole();
                role.Name = "PowerUser";
                await _roleManager.CreateAsync(role);

                returnstring += "Rolle PowerUsers war nicht da, wurde erzeugt \n";
            }
            return Content(returnstring, "text/html");
        }
		
		
		
		
		/// <summary>
        /// GET: /Users/Create
        /// Only for Authorized Users (Admins)
        /// </summary>

        [Authorize(Roles = "Admins")]
        public IActionResult Create()
        {
            return View();
        }



        /// <summary>
        /// POST /Users/Create 
        /// can be invoked only for POST requests
        /// tries to create a new User
        /// </summary>
        /// <param name="userVM"></param>
        /// <returns></returns>
        
        [Authorize(Roles = "Admins")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(UserVM uvm)
        {
            string returnstring = "Fehler";
			if (ModelState.IsValid)
            {
                // check if only one role is activated
				
				if( (uvm.IsAdmin & uvm.IsPU) | (uvm.IsAdmin & uvm.IsRO) | (uvm.IsPU & uvm.IsRO) )
					return Content("Nur eine Rolle erlaubt! Bitte neu", "text/html");
				
				var user = new AppUsers();
                user.UserName = uvm.UserModel.UserName;

                IdentityResult chkUser = await _userManager.CreateAsync(user, uvm.password);

                // Add New User to correct role   
                if (chkUser.Succeeded)
                {
                    returnstring = "User wurde erfolgreich erzeugt \n";

                    if (uvm.IsAdmin)
                    {
                        IdentityResult result1 = await _userManager.AddToRoleAsync(user, "Admins");
                    }

                    if (uvm.IsPU)
                    {
                        var result1 = await _userManager.AddToRoleAsync(user, "PowerUser");
                    }

                    if (uvm.IsRO)
                    {
                        var result1 = await _userManager.AddToRoleAsync(user, "ReadOnlyUser");
                    }
                }
				
                return Content(returnstring, "text/html");
            }
            
            // if model is not valid / validation fails: show same page
            return View(uvm);
        }
		
		
		
		/// <summary>
        /// GET: /Users/Delete/asdf_USER_id
        /// Only for Authorized Users (Admins)
        /// Show user which shall be deleted
		/// NOTE: UserModel is ok here. no uvm needed.
		
        /// </summary>
        /// <param name="id">user Id for the user to be Deleted</param>

        [Authorize(Roles = "Admins")]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.Users
                .SingleOrDefaultAsync(u => u.Id == id);

            if (user == null)
            {
                return NotFound();
            }
			
            return View(user);
        }



        /// POST: /Users/DeleteConfirmed/5
        /// Only for Authorized Users (Admins)
        /// Deletes the User
        /// Note: ActionName("Delete") makes this action accessible in the Delete Form
        /// with id
        /// </summary>
        /// <param name="id">User Id for the user to be Deleted</param>
        /// <returns></returns>

        [HttpPost]
        [ActionName("Delete")]
        [Authorize(Roles = "Admins")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var user = await _userManager.Users.SingleOrDefaultAsync(u => u.Id == id);
            await _userManager.DeleteAsync(user);
            return RedirectToAction("IndexUsers");
        }


    }
}

