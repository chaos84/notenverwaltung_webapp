Note: Version Number in footer of _shared: 3 Layout-files.

V 0.0.99 / 08.07.2019
1st Beta to Doris

V 0.1.00 / 12.08.2019
1st Release Verion 

- Sheets in Recycler (Windows App) with ID=-2 not shown anymore
- Sorting of Sheets after Orchestra and then after Inventory Number
- Added OrderNumber in Orchestra CRUD Functions
- Sorting of Orchestras (Select-List and Orchestra CRUD) after OrderNumber
- Added Version Number in Footer
